pipeline {
    agent none
    environment {
        CI = 'true'
        SONAR_TOKEN = credentials('SONAR_TOKEN')
        SONAR_HOST = credentials('SONAR_HOST')
        CI_JOB_TOKEN = credentials('CI_JOB_TOKEN')

        NPM_BUILD_COMMAND = 'npm run build'
        NPM_TEST_COMMAND = 'npm run test'
        NPM_LINT_SCAN_COMMAND = 'npm run lint'
        NPM_SONAR_SCAN_COMMAND = 'npm run sonar'
        NPM_DIST_COMMAND = 'npm run dist'

        PROJECT_ID = '29365599'
    }
    options { 
        preserveStashes()
        skipDefaultCheckout true
        gitLabConnection('arimac_gitlab')
        gitlabBuilds(builds: ['Build MR package', 'Test built package', 'Check code quality', 'Check sonar quality gate', 'Check merge capability', 'Publishing Build', 'Build publishing package', 'Publish'])
        office365ConnectorWebhooks([[
                    startNotification: true,
                        url: 'https://arimac.webhook.office.com/webhookb2/f604f803-c41e-412a-807e-66f80c660899@921c3153-cf23-4179-a51c-4673a6bfde3f/JenkinsCI/650ba128d62f480ab10661ddcaa8df29/62de516a-9483-455c-8468-1863fb75e3da'
            ]]
        )
    }

    stages {
        stage('Build MR package') {
            when { branch 'MR-*' }
            agent {
                docker { image 'node:lts-buster-slim' }
            }
            steps {
                script {
                  try {
                     checkout scm
                     updateGitlabCommitStatus name: 'Build MR package', state: 'pending'
                     sh 'npm install'
                     sh '${NPM_BUILD_COMMAND}'
                     stash includes: '**/build/*', name: 'build_stash' 
                     updateGitlabCommitStatus name: 'Build MR package', state: 'success'
                    } catch (Exception e) {
                     office365ConnectorSend webhookUrl: 'https://arimac.webhook.office.com/webhookb2/f604f803-c41e-412a-807e-66f80c660899@921c3153-cf23-4179-a51c-4673a6bfde3f/JenkinsCI/650ba128d62f480ab10661ddcaa8df29/62de516a-9483-455c-8468-1863fb75e3da',
                     message: 'CI step: Bulding MR packages failed.', 
                     status: 'Failed' 
                     updateGitlabCommitStatus name: 'Build MR package', state: 'failed'
                     throw e
                   }
                }   
            }
        }
        stage('Test built package') {
            when { branch 'MR-*' }
            agent {
                docker { image 'node:lts-buster-slim' }
            }
            steps {
                script {
                  try {
                     updateGitlabCommitStatus name: 'Test built package', state: 'pending'
                     unstash 'build_stash'
                     sh '${NPM_TEST_COMMAND}'
                     updateGitlabCommitStatus name: 'Test built package', state: 'success'
                    } catch (Exception e) {
                     office365ConnectorSend webhookUrl: 'https://arimac.webhook.office.com/webhookb2/f604f803-c41e-412a-807e-66f80c660899@921c3153-cf23-4179-a51c-4673a6bfde3f/JenkinsCI/650ba128d62f480ab10661ddcaa8df29/62de516a-9483-455c-8468-1863fb75e3da',
                     message: 'CI step: Testing built packages failed.', 
                     status: 'Failed' 
                     updateGitlabCommitStatus name: 'Test built package', state: 'failed'
                     throw e
                   }
                } 
            }
        }
        stage('Check code quality') {
            when { branch 'MR-*' }
            agent {
                docker { image 'node:lts-buster-slim' }
            }
            steps {
                script {
                  try {
                     updateGitlabCommitStatus name: 'Check code quality', state: 'pending'
                     unstash 'build_stash'
                     sh '${NPM_LINT_SCAN_COMMAND}'
                     updateGitlabCommitStatus name: 'Check code quality', state: 'success'
                    } catch (Exception e) {
                     office365ConnectorSend webhookUrl: 'https://arimac.webhook.office.com/webhookb2/f604f803-c41e-412a-807e-66f80c660899@921c3153-cf23-4179-a51c-4673a6bfde3f/JenkinsCI/650ba128d62f480ab10661ddcaa8df29/62de516a-9483-455c-8468-1863fb75e3da',
                     message: 'CI step: Checking code quality failed.', 
                     status: 'Failed' 
                     updateGitlabCommitStatus name: 'Check code quality', state: 'failed'
                     throw e
                   }
                } 
            }
        }
        stage('Check sonar quality gate') {
            when { branch 'MR-*' }
            agent {
                docker { image 'node:lts-buster-slim' }
            }
            steps {
                script {
                  try {
                     updateGitlabCommitStatus name: 'Check sonar quality gate', state: 'pending'
                     unstash 'build_stash'
                     withSonarQubeEnv('DU Dev Sonarqube') {
                        sh '${NPM_SONAR_SCAN_COMMAND}'
                     }
                     waitForQualityGate abortPipeline: true
                     updateGitlabCommitStatus name: 'Check sonar quality gate', state: 'success'
                    } catch (Exception e) {
                     office365ConnectorSend webhookUrl: 'https://arimac.webhook.office.com/webhookb2/f604f803-c41e-412a-807e-66f80c660899@921c3153-cf23-4179-a51c-4673a6bfde3f/JenkinsCI/650ba128d62f480ab10661ddcaa8df29/62de516a-9483-455c-8468-1863fb75e3da',
                     message: 'CI step: Checking sonar quality gate failed.', 
                     status: 'Failed' 
                     updateGitlabCommitStatus name: 'Check sonar quality gate', state: 'failed'
                     throw e
                   }
                } 
            }
        }
        stage('Check merge capability') {
            when { branch 'MR-*' }
            agent any    
            steps {
                script {
                  try {
                    updateGitlabCommitStatus name: 'Check merge capability', state: 'pending'
                    sh "ls -lat"
                    sh 'git merge origin/development --ff-only'
                    echo 'Merge possibility status'
                    updateGitlabCommitStatus name: 'Check merge capability', state: 'success'
                    office365ConnectorSend webhookUrl: 'https://arimac.webhook.office.com/webhookb2/f604f803-c41e-412a-807e-66f80c660899@921c3153-cf23-4179-a51c-4673a6bfde3f/JenkinsCI/650ba128d62f480ab10661ddcaa8df29/62de516a-9483-455c-8468-1863fb75e3da',
                    message: 'All CI steps passed & merge request can be merged.', 
                    status: 'Success'           
                    } catch (Exception e) {
                    office365ConnectorSend webhookUrl: 'https://arimac.webhook.office.com/webhookb2/f604f803-c41e-412a-807e-66f80c660899@921c3153-cf23-4179-a51c-4673a6bfde3f/JenkinsCI/650ba128d62f480ab10661ddcaa8df29/62de516a-9483-455c-8468-1863fb75e3da',
                    message: 'Previous CI steps passed, but the request can not be merged.', 
                    status: 'Failed'       
                    updateGitlabCommitStatus name: 'Check merge capability', state: 'failed'
                    throw e
                   }
                }  
            }
    	}

        stage('Build publishing package') {
            when { branch 'development' }
            agent {
                docker { image 'node:lts-buster-slim' }
            }
            steps {
                script {
                  try {
                    checkout scm
                    updateGitlabCommitStatus name: 'Build publishing package', state: 'pending'
                    sh 'npm install'
                    sh '${NPM_DIST_COMMAND}'
                    stash includes: '**/dist/*', name: 'dist_stash' 
                    updateGitlabCommitStatus name: 'Build publishing package', state: 'success'
                    } catch (Exception e) {
                    office365ConnectorSend webhookUrl: 'https://arimac.webhook.office.com/webhookb2/f604f803-c41e-412a-807e-66f80c660899@921c3153-cf23-4179-a51c-4673a6bfde3f/JenkinsCI/650ba128d62f480ab10661ddcaa8df29/62de516a-9483-455c-8468-1863fb75e3da',
                    message: 'CD step: Building publishing packages failed.', 
                    status: 'Failed'     
                    updateGitlabCommitStatus name: 'Build publishing package', state: 'failed'
                    throw e
                   }
                }  
            }
        }
        stage('Publish') {
            when { branch 'development' }
            agent {
                docker { image 'node:lts-buster-slim' }
            }
            steps {
                script {
                  try {
                    updateGitlabCommitStatus name: 'Publish', state: 'pending'
                    unstash 'dist_stash'
                    sh 'ls -la'
                    sh 'echo '//gitlab.com/api/v4/projects/${PROJECT_ID}/packages/npm/:_authToken=${CI_JOB_TOKEN}'>.npmrc'
                    sh 'npm publish'
                    updateGitlabCommitStatus name: 'Publish', state: 'success'
                    office365ConnectorSend webhookUrl: 'https://arimac.webhook.office.com/webhookb2/f604f803-c41e-412a-807e-66f80c660899@921c3153-cf23-4179-a51c-4673a6bfde3f/JenkinsCI/650ba128d62f480ab10661ddcaa8df29/62de516a-9483-455c-8468-1863fb75e3da',
                    message: 'Successfully published to NPM registry.', 
                    status: 'Success' 
                    } catch (Exception e) {
                    office365ConnectorSend webhookUrl: 'https://arimac.webhook.office.com/webhookb2/f604f803-c41e-412a-807e-66f80c660899@921c3153-cf23-4179-a51c-4673a6bfde3f/JenkinsCI/650ba128d62f480ab10661ddcaa8df29/62de516a-9483-455c-8468-1863fb75e3da',
                    message: 'Publishing to NPM registry failed.', 
                    status: 'Failed'      
                    updateGitlabCommitStatus name: 'Publish', state: 'failed'
                    throw e
                   }
                } 
            }
        }
    } 
}
